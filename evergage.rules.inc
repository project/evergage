<?php
/**
 * @file
 * Rules integration for the Evergage Drupal module.
 */

/**
 * Implements hook_rules_action_info().
 */
function evergage_rules_action_info() {
  // Action to add a contact to Evergage.
  $actions['evergage_rules_track'] = array(
    'label' => t('Modify information being sent over Evergage'),
    'group' => t('Evergage'),
    'access callback' => 'evergage_rules_access',
  );
  // Provide the parameters to Evergage.
  foreach (evergage_parameters() as $name => $type) {
    $actions['evergage_rules_track']['parameter'][$name] = array(
      'type' => $type,
      'label' => $name,
      'allow null' => TRUE,
      'optional' => TRUE,
    );
  }
  return $actions;
}

/**
 * Implements hook_rules_event_info().
 */
function evergage_rules_event_info() {
  $events['evergage_track'] = array(
    'label' => t('An action is being tracked with Evergage'),
    'group' => t('Evergage'),
  );
  foreach (evergage_parameters() as $name => $type) {
    $events['evergage_track']['variables'][$name] = array(
      'type' => $type,
      'label' => $name,
      'description' => t('When tracking an event with Evergage, send this information for the %name field.', array(
        '%name' => $name,
      )),
    );
  }
  return $events;
}

/**
 * Rules callback; Injects the given parameters into Evergage.
 */
function evergage_rules_track() {
  // Match the parameters to what was passed in through the Rules.
  $parameters = evergage_parameters();
  $args = func_get_args();
  $i = 0;
  foreach ($parameters as $name => $type) {
    if (isset($args[$i])) {
      $parameters[$name] = trim($args[$i]);
    }
    else {
      unset($parameters[$name]);
    }
    $i++;
  }

  // Filter out any empty arguments and .
  $parameters = array_filter($parameters);
  evergage_add_action($parameters['setAction'], $parameters);
}

/**
 * Rules callback; Determines if the user has access to use the rule.
 */
function evergage_rules_access() {
  return user_access('administer evergage');
}
