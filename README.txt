TODO: jQuery Plugin

Fields
------
uid: The user's unique ID
userId: The user's name
mail: The user's email
cid: The comment ID
nid: The node ID
title: The comment or node title
fid: When flagging a node (bookmark), the flag ID
flag_type: Bookmark/Voting/etc
vid: The taxonomy vocabulary ID
type: The type of node the user is interacting with
taxonomy_name: The name of the term the user has created
taxonomy_description: A quick description of the term the user has made
tid: The taxonomy ID that the user is interacting with
