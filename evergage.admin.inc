<?php

/**
 * @file
 * Administration settings for the Evergage Drupal module.
 */

/**
 * Menu callback; The administration settings for the Evergage module.
 */
function evergage_admin_settings() {
  $form['evergage_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Account'),
    '#description' => t('If you log into Evergage via http://myaccount.apptegic.com , then provide <em>myaccount</em>.'),
    '#default_value' => variable_get('evergage_account', ''),
  );
  $form['evergage_dataset'] = array(
    '#type' => 'textfield',
    '#title' => t('Dataset'),
    '#description' => t('The dataset to use with this site.'),
    '#default_value' => variable_get('evergage_dataset', ''),
  );
  $form['settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 50,
  );

  $form['messaging'] = array(
    '#type' => 'fieldset',
    '#title' => t('Smart Messaging'),
    '#description' => t('Configure Smart Messaging settings for Evergage.'),
    '#group' => 'settings',
  );
  $form['messaging']['evergage_onmessage'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Smart Messaging'),
    '#default_value' => variable_get('evergage_onmessage', FALSE),
  );
  $form['messaging']['evergage_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('API Token'),
    '#description' => t('Required only if you wish to use secure Smart Messages.'),
    '#default_value' => variable_get('evergage_api_token', ''),
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#description' => t('Configure additional settings for Evergage.'),
    '#group' => 'settings',
  );
  $form['advanced']['evergage_company'] = array(
    '#type' => 'textfield',
    '#title' => t('Company field'),
    '#description' => t('The name of the field which will be interpreted as the company for the user.'),
    '#default_value' => variable_get('evergage_company', ''),
  );
  $form['advanced']['evergage_track_hooks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Hook Tracking'),
    '#default_value' => variable_get('evergage_track_hooks', FALSE),
  );
  $form['advanced']['evergage_set_use_site_config'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Site Config'),
    '#default_value' => variable_get('evergage_set_use_site_config', TRUE),
  );
  $form['advanced']['evergage_cookie_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie Domain'),
    '#description' => t('Allow the Evergage user information to be held across the given cookie domain.'),
    '#default_value' => variable_get('evergage_cookie_domain', ''),
  );
  return system_settings_form($form);
}
