/**
 * @file
 * Javascript Evergage integration.
 */

var _aaq = _aaq || [];

/* $Id$ */
(function ($, window, document) {

  Drupal.behaviors.evergage = {
    attach:function (context, settings) {
      // Initialize the Apptegic account information.
      $("body").once('evergage', function () {

        var evergageAccount = settings.evergage['evergage_account'];
        var evergageDataset = settings.evergage['evergage_dataset'];

        // track popups and other single-page changes
        window._aaq.push(['setTreatHashChangeAsPageLoad', 'true']);

        window._aaq.push(['setEvergageAccount', evergageAccount], ['setDataset', evergageDataset]);

        var host = 'cdn';
        if (evergageAccount == 'demo4' || evergageAccount == 'demo3') {
            host = evergageAccount;
        }

        var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.defer = true;
        g.async = true;
        g.src = document.location.protocol + '//' + host + '.evergage.com/beacon/'
          + evergageAccount + '/' + evergageDataset + '/scripts/evergage.min.js';
        s.parentNode.insertBefore(g, s);
      });
    }
  };

})(jQuery, window, document);
